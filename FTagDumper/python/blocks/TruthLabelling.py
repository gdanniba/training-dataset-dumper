from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock


@dataclass
class TruthLabelling(BaseBlock):
    """
    Re-run the FTAG truth labelling.

    Detailed FTAG truth variables, such as `ftagTruthOriginLabel` and `ftagTruthVertexIndex`
    are normally produced when running derivations. With this block you can reproduce these
    labels as part of your dumping job, for example if the labelling code has been updated
    and you don't want to run expensive derivations.

    For more info see `TruthParticleDecoratorAlg` and `TrackTruthDecoratorAlg` in Athena,
    and [this page](https://ftag.docs.cern.ch/algorithms/labelling/track_labels/) in the 
    FTAG docs.

    Parameters
    ----------
    suffix : str
        Suffix to append to the truth labelling variables.

    Examples
    --------
    ```json
    {
        "ca_blocks": [{"block": "TruthLabelling", "suffix": "tdd"}]
    }
    ```

    Remember to add the new `ftagTruthOriginLabelTDD` etc variables to your output variables,
    or alternatively if you want to avoid the presence of new variable names in your h5 you 
    can remap variable names as follows:

    ```json
    "tracks": [
        {
            ...
            "edm_names": {
                "ftagTruthOriginLabel": "ftagTruthOriginLabelTDD",
                "ftagTruthVertexIndex": "ftagTruthVertexIndexTDD"
            }
        }
    ]
    ```
    """
    suffix: str

    def to_ca(self):
        ca = ComponentAccumulator()
        trackTruthOriginTool = CompFactory.InDet.InDetTrackTruthOriginTool(
            isFullPileUpTruth=False
        )
        ca.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.TruthParticleDecoratorAlg(
                "TruthParticleDecoratorAlg",
                trackTruthOriginTool=trackTruthOriginTool,
                ftagTruthOriginLabel="ftagTruthOriginLabel" + self.suffix,
                ftagTruthVertexIndex="ftagTruthVertexIndex" + self.suffix,
                ftagTruthTypeLabel="ftagTruthTypeLabel" + self.suffix,
                ftagTruthSourceLabel="ftagTruthSourceLabel" + self.suffix,
                ftagTruthParentBarcode="ftagTruthParentBarcode" + self.suffix,
            )
        )
        ca.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.TrackTruthDecoratorAlg(
                "TrackTruthDecoratorAlg",
                trackTruthOriginTool=trackTruthOriginTool,
                truthLeptonTool=CompFactory.TruthClassificationTool(
                    "TruthClassificationTool"),
                acc_ftagTruthVertexIndex="ftagTruthVertexIndex" + self.suffix,
                acc_ftagTruthTypeLabel="ftagTruthTypeLabel" + self.suffix,
                acc_ftagTruthSourceLabel="ftagTruthSourceLabel" + self.suffix,
                acc_ftagTruthParentBarcode="ftagTruthParentBarcode" + self.suffix,
                dec_ftagTruthOriginLabel="ftagTruthOriginLabel" + self.suffix,
                dec_ftagTruthVertexIndex="ftagTruthVertexIndex" + self.suffix,
                dec_ftagTruthTypeLabel="ftagTruthTypeLabel" + self.suffix,
                dec_ftagTruthSourceLabel="ftagTruthSourceLabel" + self.suffix,
                dec_ftagTruthBarcode="ftagTruthBarcode" + self.suffix,
                dec_ftagTruthParentBarcode="ftagTruthParentBarcode" + self.suffix,
            )
        )
        return ca
