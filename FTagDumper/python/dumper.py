
"""
Python interface for the FTagDumper package.

"""

import argparse
import os
from pathlib import Path
import json

from GaudiKernel.Configurable import DEBUG
from AthenaConfiguration.AllConfigFlags import initConfigFlags
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AnalysisAlgorithmsConfig.ConfigText import combineConfigFiles

from FTagDumper.block_factory import BlockFactory

class DumperHelpFormatter(
    argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter
):
    ...


def base_parser(description, add_help=True):
    parser = argparse.ArgumentParser(description=description, formatter_class=DumperHelpFormatter, add_help=add_help)
    parser.add_argument(
        "input_files", nargs="+",
        help="comma or space separated list of input filenames")
    parser.add_argument(
        "-o", "--output", default=Path("output.h5"), type=Path,
        help="output filename")
    parser.add_argument(
        "-c", "--config-file", required=True, type=Path,
        help="job configuration file")
    parser.add_argument(
        "-m", "--max-events",
        type=int, nargs="?",
        help="number of events to process")
    parser.add_argument(
        "-i", "--event-print-interval",
        type=int, default=500,
        help="set output frequency")
    parser.add_argument(
        '-p', '--force-full-precision',
        action='store_true',
        help='force all half-precision outputs to full precision')
    parser.add_argument(
        '--FPEAuditor',
        action='store_true',
        help='error if floating point exception occurs (requires Athena)')
    parser.add_argument(
        "-d", "--debug",
        action="store_true",
        help="set output level to DEBUG")
    parser.add_argument(
        "-g", "--debugger",
        action="store_true",
        help="attach debugger at execute step")
    return parser


def update_flags(args, flags=None):

    if flags is None:
        flags = initConfigFlags()

    # parse input files
    if len(args.input_files) > 1:
        if any("," in f for f in args.input_files):
            raise ValueError(
                "you provided a mix of spaces and commas"
                " in your input file list, this is probably an error"
            )
        flags.Input.Files = [str(x) for x in args.input_files]
    else:
        flags.Input.Files = str(args.input_files[0]).rstrip(",").split(",")

    # set number of events to process
    if args.max_events:
        flags.Exec.MaxEvents = args.max_events

    # set output level
    if args.debug:
        flags.Exec.OutputLevel = DEBUG

    # attach debugger
    if args.debugger:
        prepGdb()
        flags.Exec.DebugStage = "init"

    # error on floating point exceptions
    if args.FPEAuditor:
        flags.Exec.FPE = -1

    return flags


def prepGdb():
    """
    Running gdb is sometimes more difficult than it should be, this
    runs some workarounds for the debugger in images.
    """
    # The environment variable I'm checking here just happens to be
    # set to a directory you would not have access to outside images.
    if os.environ.get('LCG_RELEASE_BASE') == '/opt/lcg':
        # we used to call `del os.environ['PYTHONHOME']` here, but as
        # of 24.2.38 (el9) the image is happier if we don't. So this
        # block is a noop for now, but we'll leave it in in case some
        # hacks are needed.
        pass


def getMainConfig(args, flags):
    ca = MainServicesCfg(flags)
    ca.merge(PoolReadCfg(flags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))
    # Avoid stack traces to the exception handler. These traces
    # aren't very useful since they just point to the handler, not
    # the original bug.
    ca.addService(CompFactory.ExceptionSvc(Catch="NONE"))
    return ca


def setMainAlgSeqToSeqAND(ca, mainSequence="AthAlgSeq"):
    # Make the AthAlgSeq sequence (the default sequence where algs are running)
    # of type "SEQ AND", squential with AND logic. IgnoreFilterPassed and
    # StopOverride settings are necessary for checking the filter status of
    # algorithms and stop the algorithm sequence if filter is false.
    algSeq = ca.getSequence(mainSequence)
    algSeq.ModeOR = False
    algSeq.Sequential = True
    algSeq.IgnoreFilterPassed = False
    algSeq.StopOverride = False
    return


def combinedConfig(config_file):
    with open(config_file, 'r') as cfg:
        config_dict = json.load(cfg)
    combineConfigFiles(
        config_dict,
        config_file.parent,
        fragment_key='file')
    return config_dict


def getBlocksConfig(flags, args):
    config_dict = combinedConfig(args.config_file)
    ca = ComponentAccumulator()
    for block in BlockFactory(config_dict, flags):
        ca.merge(block.to_ca())
    return ca


def getDumperConfig(args, config_dict=None):
    ca = ComponentAccumulator()

    output = CompFactory.H5FileSvc(path=str(args.output))
    ca.addService(output)

    if config_dict is None:
        config_dict = combinedConfig(args.config_file)

    if "dumper" in config_dict:
        config_dict = config_dict["dumper"]
    if "ca_blocks" in config_dict:
        config_dict.pop("ca_blocks")

    ca.addEventAlgo(
        CompFactory.JetDumperAlg(
            name=f'{args.config_file.stem}DatasetDumper',
            configJson=json.dumps(config_dict),
            output=output,
            forceFullPrecision=args.force_full_precision,
        )
    )

    return ca
