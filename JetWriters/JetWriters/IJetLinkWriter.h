#ifndef IJET_ASSOCIATION_WRITER_H
#define IJET_ASSOCIATION_WRITER_H

#include "xAODJet/JetFwd.h"

class IJetLinkWriter
{
public:
  virtual ~IJetLinkWriter(){};
  virtual void write(const xAOD::Jet& jet) = 0;
  virtual void flush() = 0;
};

#endif
